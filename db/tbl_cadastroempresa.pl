:- use_module(library(persistency)).

:- persistent
    empresa(
    razao_social:atom,
    identificacao:atom,
    tipo_de_pessoa: atom,
    cnpj:positive_integer,
    iscricao_estadual:atom,
    inscricao_municipal:positive_integer,
    endereco: atom,
    bairro: atom,
    municipio: atom,
    cep:positive_integer,
    uf:atom,
    telefone:positive_integer,
    email: atom,).

:-persistency
    titular(nome: atom,
        cpf:positive_integer,
        funcao: atom).

:- initialization(db_attach('tbl_cadastroempresa.pl', [])).