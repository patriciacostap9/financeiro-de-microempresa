:- use_module(library(persistency)).

:- persistent
    baixa_contas_a_receber(
    empresa:atom,
    conta_banco: atom,
    forma_de_pagamento: atom,
    data_de_vencimento:positive_integer,
    data_de_baixa:positive_integer,
    numero_do_documento:positive_integer,
    valor_recebido:positive_integer,
    valor_residual:positive_integer).


:- initialization(db_attach('tbl_baixacontasreceber.pl', [])).