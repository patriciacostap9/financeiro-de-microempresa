:- use_module(library(persistency)).

:- persistent
    contas_a_receber(
    empresa:atom,
    data_de_vencimento:positive_integer,
    cliente:atom,
    data_de_emissao:positive_integer,
    numero_do_documento:positive_integer,
    valor_do_titlo:positive_integer).


:- initialization(db_attach('tbl_contasareceber.pl', [])).