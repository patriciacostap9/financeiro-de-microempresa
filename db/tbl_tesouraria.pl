:- use_module(library(persistency)).

:- persistent
    tesouraria(
    empresa:atom,
    cliente_fornecedor:atom,
    conta_de_entrada: atom,
    conta_de_saida:atom,
    valor:positive_integer,
    forma_de_pagamento:atom,
    numero_documento:positive_integer,
    data_emissao: positive_integer,
    data_vencimento:positive_integer,
    data_disponibilidade:positive_integer).


:- initialization(db_attach('tbl_tesouraria.pl', [])).