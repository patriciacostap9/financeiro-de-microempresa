:- use_module(library(persistency)).

:-persistency
    conta_bancaria(classificacao: positive_integer,
        descricao: atom,
        numero_da_conta:positive_integer,
        numero_da_agencia:positive_integer,
        data_inicial:positive_integer,
        saldo_inicial:positive_integer).

:-persistency
    caracteristica(caixa:(),
    banco()).

:- initialization(db_attach('tbl_cadastrocontabancaria.pl', [])). 