:- use_module(library(persistency)).

:- persistent
    plano de contas(classicacao:positive_integer,
    tipo_conta: atom,
    descricao: atom).

:- persistent
    caracteristicas(caixa:(),
    cliente:(),
    entrada_de_recursos(),
    banco:(),
    fornecedor(),
    saida_de_recursos:()).

:- initialization(db_attach('tbl_planodecontas.pl', [])).