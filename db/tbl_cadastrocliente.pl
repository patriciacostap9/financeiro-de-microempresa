:- use_module(library(persistency)).

:- persistent
    cliente(
    razao_social:atom,
    identificacao:atom,
    classificacao:positive_integer,
    tipo_de_pessoa: atom,
    cnpj/cpf:positive_integer,
    iscricao_estadual:_,
    inscricao_municipal:_,
    endereco: atom,
    bairro: atom,
    municipio: atom,
    cep:positive_integer,
    uf:atom,
    telefone:positive_integer,
    email: atom,).

:-persistency
    titular(nome_titular: atom,
        cpf_titular:positive_integer,
        funcao_titular: atom).

:- initialization(db_attach('tbl_cadastrocliente.pl', [])).