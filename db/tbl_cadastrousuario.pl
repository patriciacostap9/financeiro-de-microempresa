:- use_module(library(persistency)).

:- persistent
    usuario(
    nome:atom,
    usuario:atom,
    senha: atom,
    confirma_senha:atom).

:- persistent
        permissoes(usuario(),
        empresa(),
        plano_de_contas(),
        clientes(),
        fornecedores(),
        forma_de_pagamento(),
        conta_bancaria()).

:- initialization(db_attach('tbl_cadastrousuario.pl', [])).